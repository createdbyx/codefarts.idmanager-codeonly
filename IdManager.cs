﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.IdManager
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    ///     Provides a manager for unique integer based id's.
    /// </summary>
    public class IdManager
    {
        #region Fields

        /// <summary>
        ///     Holds the list of generated and registered id's.
        /// </summary>
        private readonly List<long> idList = new List<long>();

        /// <summary>
        ///     Used for thread locking.
        /// </summary>
        private readonly object obj = new object();

        /// <summary>
        ///     Holds a list of pre-generated id values.
        /// </summary>
        private List<long> availableList = new List<long>();

        /// <summary>
        ///     Holds the value for the <see cref="BufferCount" /> property.
        /// </summary>
        private int bufferCount;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IdManager"/> class.
        /// </summary>
        /// <param name="bufferCount">
        /// Sets the initial value of the <see cref="BufferCount"/> property.
        /// </param>
        public IdManager(int bufferCount)
        {
            this.BufferCount = bufferCount;
            this.Reset();
        }

        /// <summary>Initializes a new instance of the <see cref="IdManager" /> class.</summary>
        public IdManager()
            : this(1000)
        {
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets or sets the buffer count that controls how many available id's are pre-generated when more id's are needed.
        /// </summary>
        public int BufferCount
        {
            get
            {
                return this.bufferCount;
            }

            set
            {
                if (value < 1)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                this.bufferCount = value;
            }
        }

        /// <summary>
        ///     Gets the registered ids.
        /// </summary>
        public ReadOnlyCollection<long> RegisteredIds
        {
            get
            {
                return new ReadOnlyCollection<long>(this.idList);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>Gets a unique new identifier.</summary>
        /// <returns>THe value of the newly allocated identifier.</returns>
        public long NewId()
        {
            lock (this.obj)
            {
                if (this.availableList.Count > 0)
                {
                    var item = this.availableList[0];
                    this.availableList.RemoveAt(0);
                    this.idList.Add(item);
                    return item;
                }

                var max = this.idList.Max();

                for (var i = 1; i < this.bufferCount; i++)
                {
                    this.availableList.Add(max + i);
                }

                this.availableList = this.availableList.Except(this.idList).ToList();

                return this.NewId();
            }
        }

        /// <summary>
        /// Registers the identifier.
        /// </summary>
        /// <param name="id">
        /// A pre-existing identifier.
        /// </param>
        public void RegisterId(long id)
        {
            this.RegisterId(new[] { id });
        }

        /// <summary>
        /// Registers the identifier.
        /// </summary>
        /// <param name="ids">
        /// The id's to be registered.
        /// </param>
        public void RegisterId(IEnumerable<long> ids)
        {
            lock (this.obj)
            {
                var distinct = ids.Except(this.idList);
                this.idList.AddRange(distinct);
                this.availableList = this.availableList.Except(this.idList).ToList();
            }
        }

        /// <summary>
        ///     Resets the id manager by removing all registered id's and clearing the buffered available list.
        /// </summary>
        public void Reset()
        {
            lock (this.obj)
            {
                this.availableList.Clear();
                this.idList.Clear();
                for (var i = 0; i < this.bufferCount; i++)
                {
                    this.availableList.Add(i);
                }
            }
        }

        #endregion

        /// <summary>Releases the identifier.</summary>
        /// <param name="id">The identifier.</param>
        public void ReleaseId(long id)
        {
            lock (this.obj)
            {
                this.idList.Remove(id);
                this.availableList.Add(id);
            }
        }
    }
}